//
//  SGMobileServerVMV.m
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 26/10/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import "SGMobileServerVMV.h"
#import "SGLAMOSELMainBridge.h"
#import "UIURLAlertView.h"
#import <CommonCrypto/CommonDigest.h>

#import "NSMutableArray+QueueAdditions.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code"

NSString *LAMOSELLocalizedString(NSString* key, NSString* comment) 
{
	static NSBundle* bundle = nil;
	
	if (!bundle) 
	{
		NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Lamosel.bundle"];
		bundle = [[NSBundle bundleWithPath:path] retain];
	}
	
	return [bundle localizedStringForKey:key value:key table:@"LAMOSEL"];
}

@implementation SGMobileServerVMV

@synthesize repeatingTimer, qualifiedTimer;
@synthesize vmv;
@synthesize currentVersion, alerting;
@synthesize lamoselDict;
@synthesize vmvDelegate;

@synthesize timeBeforePopUp, fifoMessagesLimit, qualifiedOpenings, minYTime, maxZtime, versioningRecurrence;

@synthesize mustSkipcacheOnVMVCall;

#pragma mark - Initialization

- (id)init 
{
    if (self = [super init]) 
	{	
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callToMobileServerDidReceiveData:) name:@"SGMobileServerBridgeDidSucceedRequest" object:self];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callToMobileServerDidConnectSuccessfully:) name:@"SGMobileServerBridgeDidFaileRequest" object:self];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callToMobileServerDidSucceed:) name:@"SGMobileServerBridgeDidSucceedRequest" object:self];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callToMobileServerDidFail:) name:@"SGMobileServerBridgeDidFaileRequest" object:self];
		
		versioningShown = NO;
		
		[self loadData];
		
		isAlreadyDoingSomething = NO;
		
		[self setTimeBeforePopUp:kTimeBeforePopUp];
		[self setFifoMessagesLimit:kFIFOMessagesLimit];
		[self setQualifiedOpenings:kQualifiedOpenings];
		[self setMinYTime:kMinYTime];
		[self setMaxZtime:kMaxZTime];
		[self setVersioningRecurrence:kVersioningRecurrence];
        
        mustSkipcacheOnVMVCall = FALSE;
	}
	
    return self;
}

#pragma mark - Memory Management

- (void)dealloc 
{
	[self saveData];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SGMobileServerBridgeDidReceiveData" object:self];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SGMobileServerBridgeDidConnectSuccesfully" object:self];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SGMobileServerBridgeDidSucceedRequest" object:self];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"SGMobileServerBridgeDidFaileRequest" object:self];
	
	[lamoselDict release];
    
	if (alerting != nil)
    {
        [alerting release];
    }
	
	[currentVersion release];
	[vmv release];
	
	[super dealloc];
}

- (void)loadData 
{
	NSDictionary *dictionary = nil;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	
	if ([paths count] > 0) 
	{
        // only copying one file
        NSString *path = [NSString stringWithFormat:@"%@/Caches/LAMOSEL_VMV",[paths objectAtIndex:0]];
		dictionary = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    }
	
	if (dictionary == nil) 
	{
		NSMutableDictionary *d = [[NSMutableDictionary alloc] init];
		[self setLamoselDict:d];
		
		if ( [lamoselDict respondsToSelector:@selector(setObject:forKey:)] )
        {
			[lamoselDict setObject:[NSDate date] forKey:@"VMV_FirstLaunch"];
		}
		
		[d release];
	}
	else 
	{
		NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
		[self setLamoselDict:d];
		[d release];
	}
}

- (void)saveData 
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	
	if ([paths count] > 0) 
	{
        // only copying one file
        NSString *path = [NSString stringWithFormat:@"%@/Caches/LAMOSEL_VMV",[paths objectAtIndex:0]];
		[NSKeyedArchiver archiveRootObject:lamoselDict toFile:path];
        [self addSkipBackupAttributeToItemAtURL:path];
    }	
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSString *)path
{
    NSURL *URL = [NSURL fileURLWithPath:path];
    id flag = nil;
    NSError *error = nil;
    [URL getResourceValue:&flag forKey:NSURLIsExcludedFromBackupKey error: &error];
    BOOL isAlreadyExclude = [flag boolValue];
    
    if (isAlreadyExclude)
    {
        return isAlreadyExclude;
    }
    else
    {
        BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        
        if ([[SGLAMOSELMainBridge sharedMainBridge] debugLogIsActive] == TRUE)
        {
            if (success)
            {
                NSLog(@"CORRECTLY SET EXCLUDED URL from iCloud Backup : %@", URL);
            }
            else
            {
                NSLog(@"ERROR SET EXCLUDED URL from iCloud Backup : %@ / %@", URL, error);
            }
        }
        
        return success;
    }
}

#pragma mark - Download reaction

- (void)callToMobileServerDidReceiveData:(NSNotification *)notification 
{
}

- (void)callToMobileServerDidConnectSuccessfully:(NSNotification *)notification 
{
}

- (void)callToMobileServerDidSucceed:(NSNotification *)notification 
{
	NSDictionary *d = [notification userInfo];
	
	if (d != nil) 
	{
		if ([d objectForKey:@"ServiceType"] != nil)  
		{
			switch ([[d objectForKey:@"ServiceType"] intValue]) 
			{
				case MSVMVInfo: 
				{
					// Because of the unusual aspect of this webservice.
					// We cannot claim to be in that case of successfull API call return.
					// We will treat the result as a success in the fail call.
					
					[self setVmv:[self checkJSON:d forKey:@"UserInfo" defaultValue:[NSDictionary dictionary]]];
					
					if (vmv == nil) 
					{
						return;
					}
					
					NSNumber *n = [self checkJSON:vmv forKey:@"exception" defaultValue:[NSNumber numberWithBool:YES]];
					
					if ([n boolValue]) 
					{
						return;
					}
					
					[self start];
				}
					break;
				default:
					break;
			}
		}		
	}
}

- (void)callToMobileServerDidFail:(NSNotification *)notification 
{
	isAlreadyDoingSomething = NO;
	NSDictionary *d = [notification userInfo];
	
	if (d != nil) 
	{
		if ([d objectForKey:@"ServiceType"] != nil) 
		{
			switch ([[d objectForKey:@"ServiceType"] intValue]) 
			{
				case MSVMVInfo: 
				{
					
				}
					break;
				default:
					break;
			}
		}
	}
	
}

#pragma mark - Rules - Versioning

- (Boolean)aNewVersionExist 
{
	return ([currentVersion compare:[self checkJSON:vmv forKey:@"versionCode" defaultValue:@"0"]] == NSOrderedAscending);
}

- (Boolean)versioningForCurrentVersionAlreadyPrinted 
{    
	NSString *versionKey = [NSString stringWithFormat:@"VMV_VersioningShowed_%@", currentVersion];
    
	// We remove any old versioning key
	
	NSMutableDictionary *lamoselDictMutable = [[NSMutableDictionary alloc] initWithDictionary: lamoselDict ];
	
	for (NSString *key in lamoselDict)
	{
		if ([key hasPrefix:@"VMV_VersioningShowed_"] && ([(NSString *)key isEqualToString:versionKey] != YES))
		{
			[lamoselDictMutable removeObjectForKey:key];
		}
	}
	
	[lamoselDict release];
	lamoselDict = lamoselDictMutable;
	
	// We initialize a value if non exist
	if ([lamoselDict objectForKey:versionKey] == nil && [lamoselDict respondsToSelector:@selector(setObject:forKey:)] )
	{
		[lamoselDict setObject:[NSDate dateWithTimeIntervalSince1970:0] forKey:versionKey];
	}
	
	return ([[lamoselDict objectForKey:versionKey] compare:(NSNumber*)[NSDate dateWithTimeIntervalSinceNow:-[[NSNumber numberWithUnsignedInt:(int)versioningRecurrence] doubleValue]]] == NSOrderedDescending);
}

- (Boolean)messageForCurrentVersionAlreadyPrinted:(NSDictionary *)message 
{
	NSArray *sa = [self checkJSON:lamoselDict forKey:@"VMV_Messages" defaultValue:[NSArray array]];
	NSMutableArray *a = [[NSMutableArray alloc] initWithArray:sa];
	
	if (a == nil) 
	{
		a = [[NSMutableArray alloc] init];
	}
	
	NSString *s = [NSString stringWithFormat:@"%@-%@-%@", [self checkJSON:message forKey:@"title" defaultValue:@""], [self checkJSON:message forKey:@"msg" defaultValue:@""], [self checkJSON:message forKey:@"url" defaultValue:@""]];
	
	if ([a containsObject:[SGMobileServerVMV md5:s]]) 
	{
		[a release];
		return YES;
	}
	else 
	{
		if ( [a count] >= fifoMessagesLimit ) 
		{
			[a dequeue];
		}
		
		[a enqueue:[SGMobileServerVMV md5:s]];
		
		if ( [lamoselDict respondsToSelector:@selector(setObject:forKey:)] ) {
			[lamoselDict setObject:a forKey:@"VMV_Messages"];
		}
		[a release];
		return NO;
	}
	
	if ( [lamoselDict respondsToSelector:@selector(setObject:forKey:)] ) {
		[lamoselDict setObject:a forKey:@"VMV_Messages"];
	}

	[a release];
	return YES;
}


#pragma mark - PopUp Handling

- (void)showVersioning
{
    /// Multi Langue
    
    NSDictionary * languages = [self checkJSON:vmv forKey:@"langue" defaultValue:nil];
    NSString *versioningURL = [self checkJSON:vmv forKey:@"marketLink" defaultValue:nil];
    
    if (languages != nil) // if multilanguage is supported
    {
        // Get current code format
        NSString * langage = [[NSLocale preferredLanguages] objectAtIndex:0];
        NSString * formatLangageForDatePicker = [NSString stringWithFormat:@"%@_%@", langage, [langage uppercaseString]];
        
        NSDictionary * language = [self checkJSON:languages forKey:formatLangageForDatePicker defaultValue:nil];
        
        if (language != nil && versioningURL != nil && [versioningURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:language forKey:@"alertTitle" defaultValue:@"N/A"]
                                                     message:[self checkJSON:language forKey:@"alertText" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Cancel", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_Update", @""), nil];
            [alerting setUrl:versioningURL];
        }
        else if (versioningURL != nil && [versioningURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:vmv forKey:@"alertTitle" defaultValue:@"N/A"]
                                                     message:[self checkJSON:vmv forKey:@"alertText" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Cancel", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_Update", @""), nil];
            [alerting setUrl:versioningURL];
        }
        else
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:vmv forKey:@"alertTitle" defaultValue:@"N/A"]
                                                     message:[self checkJSON:vmv forKey:@"alertText" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Close", @"")
                                           otherButtonTitles:nil];
        }
        
        [alerting setTag:PopUpVMVVersioningTag];
        [alerting show];
    }
    else
    {
        if (versioningURL != nil && [versioningURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:vmv forKey:@"alertTitle" defaultValue:@"N/A"]
                                                     message:[self checkJSON:vmv forKey:@"alertText" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Cancel", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_Update", @""), nil];
            [alerting setUrl:versioningURL];
        }
        else
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:vmv forKey:@"alertTitle" defaultValue:@"N/A"]
                                                     message:[self checkJSON:vmv forKey:@"alertText" defaultValue:@"N/A"]
                                                    delegate:self 
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Close", @"") 
                                           otherButtonTitles:nil];
        }
        
        [alerting setTag:PopUpVMVVersioningTag];
        [alerting show];
    }
}

- (void)showMessaging:(NSDictionary *)message
{
    /// Multi Langue
    
    NSString *messageURL = [self checkJSON:message forKey:@"url" defaultValue:nil];
    NSDictionary * languages = [self checkJSON:message forKey:@"langue" defaultValue:nil];
    
    // if multilanguage is supported
    if (languages != nil)
    {
        // Get current code format
        NSString * langage = [[NSLocale preferredLanguages] objectAtIndex:0];
        NSString * formatLangageForDatePicker = [NSString stringWithFormat:@"%@_%@", langage, [langage uppercaseString]];
        
        NSDictionary * language = [self checkJSON:languages forKey:formatLangageForDatePicker defaultValue:nil];
        
        if (language != nil && messageURL != nil && [messageURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:language forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:language forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Cancel", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_Go", @""), nil];
            [alerting setUrl:messageURL];
        }
        else if (messageURL != nil && [messageURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:message forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:message forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Cancel", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_Go", @""), nil];
            [alerting setUrl:messageURL];
        }
        else
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:message forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:message forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Close", @"")
                                           otherButtonTitles:nil];
        }
        
        [alerting setTag:PopUpVMVMessagingTag];
        [alerting show];
    }
    else
    {
        if (messageURL != nil && [messageURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:message forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:message forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Cancel", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_Go", @""), nil];
            [alerting setUrl:messageURL];
        }
        else
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:message forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:message forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self 
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Close", @"") 
                                           otherButtonTitles:nil];
        }
        
        [alerting setTag:PopUpVMVMessagingTag];
        [alerting show];
    }
}

- (void)showVoting:(NSDictionary *)voting
{
	NSString *votingURL = [self checkJSON:voting forKey:@"url" defaultValue:nil];
    NSDictionary * languages = [self checkJSON:voting forKey:@"langue" defaultValue:nil];
    
    // if multilanguage is supported
    if (languages != nil)
    {
        // Get current code format
        NSString * langage = [[NSLocale preferredLanguages] objectAtIndex:0];
        NSString * formatLangageForDatePicker = [NSString stringWithFormat:@"%@_%@", langage, [langage uppercaseString]];
        
        NSDictionary * language = [self checkJSON:languages forKey:formatLangageForDatePicker defaultValue:nil];
        
        if (language != nil && votingURL != nil && [votingURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:language forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:language forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Vote", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_NeverAsk", @""), LAMOSELLocalizedString(@"LAMOSEL_Later", @""), nil];
            [alerting setUrl:votingURL];
            
            
            
        }
        // If there's the code country is not fonud in the json file
        else if (votingURL != nil && [votingURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:voting forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:voting forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Vote", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_NeverAsk", @""), LAMOSELLocalizedString(@"LAMOSEL_Later", @""), nil];
            [alerting setUrl:votingURL];
        }
        else
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:voting forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:voting forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Close", @"")
                                           otherButtonTitles:nil];
        }
    }
    else
    {
        if (votingURL != nil && [votingURL isKindOfClass:[NSString class]])
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:voting forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:voting forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Vote", @"")
                                           otherButtonTitles:LAMOSELLocalizedString(@"LAMOSEL_NeverAsk", @""), LAMOSELLocalizedString(@"LAMOSEL_Later", @""), nil];
            [alerting setUrl:votingURL];
        }
        else
        {
            alerting = [[UIURLAlertView alloc] initWithTitle:[self checkJSON:voting forKey:@"title" defaultValue:@"N/A"]
                                                     message:[self checkJSON:voting forKey:@"msg" defaultValue:@"N/A"]
                                                    delegate:self 
                                           cancelButtonTitle:LAMOSELLocalizedString(@"LAMOSEL_Close", @"") 
                                           otherButtonTitles:nil];
        }
	}
    
	[alerting setTag:PopUpVMVVotingTag];
	[alerting show];
}

#pragma mark - Versioning-Messaging-Voting

- (void)baseVersioningMessagingVoting:(NSNumber *)contentObjectIdentifier currentVersion:(NSString *)cVersion 
{
    NSString* langCode = nil;
    
    if (vmvDelegate != nil)
	{
		if ([vmvDelegate conformsToProtocol:@protocol(SGMobileServerVMVDelegate)])
		{
			if ([vmvDelegate respondsToSelector:@selector(getLanguageCode)])
			{
				langCode = [vmvDelegate getLanguageCode];
			}
		}
	}
    
	isAlreadyDoingSomething = YES;
	
	[self setCurrentVersion:cVersion];
    
    NSString *parameters = nil;
	
   // NSString *ISO639_2LanguageCode = [[NSLocale currentLocale] ISO639_2LocaleIdentifier];

    if (mustSkipcacheOnVMVCall == TRUE)
    {
        if (langCode != nil)
        {
            parameters = [NSString stringWithFormat:@"/%@/(bundle)/%@?lang=%@&%f", contentObjectIdentifier,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"],langCode, [[NSDate date] timeIntervalSince1970]];
        }
        else
        {
            parameters = [NSString stringWithFormat:@"/%@/(bundle)/%@?%f", contentObjectIdentifier,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"], [[NSDate date] timeIntervalSince1970]];

        }
    }
    else
    {
        if (langCode != nil)
        {
            parameters = [NSString stringWithFormat:@"/%@/(bundle)/%@?lang=%@", contentObjectIdentifier,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"],langCode];
        }
        else
        {
            parameters = [NSString stringWithFormat:@"/%@/(bundle)/%@", contentObjectIdentifier,[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"]];
        }
    }
    
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", [SGMobileServerVMV baseServerURL], kMobileServerWSVMVInfo, parameters]];
	
	if ([[SGLAMOSELMainBridge sharedMainBridge] debugLogIsActive] == TRUE)
	{
		NSLog(@"\n\n--LAMOSEL: baseVersioningMessagingVoting => %@\n\n",[url absoluteString]);
	}
	
	NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:kMobileServerWSTimeout];
	[self launchConnectionForURLRequest:urlRequest withService:MSVMVInfo errorTitle:nil errorMessage:nil activityIndicator:NO];
}

- (void)start 
{
	if (vmv == nil)
	{
		return;
	}
	
	if (![vmv count]) 
	{
		return;
	}
    
	// If it's the first time, we set the value
	if ([self aNewVersionExist] && ![self versioningForCurrentVersionAlreadyPrinted]) 
	{
		[self showVersioning];
	}
	else 
	{
		NSInteger messageIndex = -1;
		
		if ((messageIndex = [self stepMessaging]) >= 0) 
		{
			[self showMessaging:[(NSArray *)[vmv objectForKey:@"messaging"] objectAtIndex:messageIndex]];
		}
		else if (!versioningShown) 
		{
			[self startVotingTimer];
		}
	}
}

- (void)startVotingTimer 
{
	if (repeatingTimer == nil) 
	{
		repeatingTimer = [NSTimer scheduledTimerWithTimeInterval:timeBeforePopUp target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:NO];
	}
}

- (void)startQualifiedTimer 
{
	if (qualifiedTimer == nil) 
	{
		qualifiedTimer = [NSTimer scheduledTimerWithTimeInterval:timeBeforePopUp target:self selector:@selector(qualifiedFireMethod:) userInfo:nil repeats:NO];
	}
}

- (void)startUserVMV:(NSNumber *)contentObjectIdentifier currentVersion:(NSString *)cVersion 
{
	[self startQualifiedTimer];
	
	if (!isAlreadyDoingSomething) 
	{
		if (vmv == nil) 
		{
			[self baseVersioningMessagingVoting:contentObjectIdentifier currentVersion:cVersion];
		}
		else 
		{
			[self start];
		}
	}
}

- (void)stopUserVMV 
{
	if (repeatingTimer != nil) 
	{
		[repeatingTimer invalidate];
		self.repeatingTimer = nil;
	}
	
	if (qualifiedTimer != nil) 
	{
		[qualifiedTimer invalidate];
		self.qualifiedTimer = nil;
	}
	
	isAlreadyDoingSomething = NO;
	versioningShown = NO;
	
	if (alerting != nil) 
	{
		if ([alerting isVisible]) 
		{
			[alerting dismissWithClickedButtonIndex:0 animated:NO];
			[alerting release], alerting = nil;
		}
	}
}

- (NSInteger)stepMessaging 
{
	if (vmv == nil) 
	{
		return NO;
	}
	
	if (![vmv count])
	{
		return NO;
	}
	
	NSInteger i=0, foundMessageIndex = -1;
	NSArray *a = [self checkJSON:vmv forKey:@"messaging" defaultValue:[NSArray array]];
	Boolean foundMessage = NO;
	
	while ((i < [a count]) && !foundMessage) 
	{
		if (![self messageForCurrentVersionAlreadyPrinted:[a objectAtIndex:i]]) 
		{
			foundMessage = YES;
			foundMessageIndex = i;
		}
		i++;
	}
	
	return foundMessageIndex;		
}

- (Boolean)stepVoting 
{
	NSArray *a = [self checkJSON:vmv forKey:@"voting" defaultValue:[NSArray array]];
	return [a count];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	if ([alertView tag] == PopUpVMVVersioningTag) 
	{
		NSString *url = @"";
		
		if ([alertView isKindOfClass:[UIURLAlertView class]]) 
		{
			url = [(UIURLAlertView *)alertView url];
		}
		
		[alerting release], alerting = nil;
		
		// Mark the popup as seen
		versioningShown = YES;
		NSString *versionKey = [NSString stringWithFormat:@"VMV_VersioningShowed_%@", currentVersion];
		
		if ( [lamoselDict respondsToSelector:@selector(setObject:forKey:)] ) {
			[lamoselDict setObject:[NSDate date] forKey:versionKey];
		}

		
		// Check what to do next
		if (buttonIndex == 1) 
		{
			// Treating case of going outside application
			[self callURLIfInternalProtocol:url];
			isAlreadyDoingSomething = NO;
		}
		else 
		{
			// The user didn't click the "go" button, so it means that we should treat messages here.
			NSInteger messageIndex = -1;
			
			if ((messageIndex = [self stepMessaging]) >= 0) 
			{
				[self showMessaging:[(NSArray *)[vmv objectForKey:@"messaging"] objectAtIndex:messageIndex]];
			}
			
			isAlreadyDoingSomething = NO;
		}
	} 
	else if ([alertView tag] == PopUpVMVMessagingTag) 
	{
		NSString *url = @"";
		
		if ([alertView isKindOfClass:[UIURLAlertView class]]) 
		{
			url = [(UIURLAlertView *)alertView url];
		}
		
		[alerting release], alerting = nil;
		
		if (buttonIndex == 1) 
		{
			[self callURLIfInternalProtocol:url];
			isAlreadyDoingSomething = NO;
		}
		else if (!versioningShown) 
		{
			[self startVotingTimer];
		}
	} 
	else if ([alertView tag] == PopUpVMVVotingTag) 
	{
		NSString *url = @"";
		
		if ([alertView isKindOfClass:[UIURLAlertView class]]) 
		{
			url = [(UIURLAlertView *)alertView url];
		}
		
		[alerting release], alerting = nil;
		
		// Call url to go outside or not if needed
		if (buttonIndex == 0) 
		{
			if ( [lamoselDict respondsToSelector:@selector(setObject:forKey:)] ) {
				[lamoselDict setObject:[NSNumber numberWithBool:YES] forKey:@"VMV_NeverAskVoting"];
			}
			[self callURLIfInternalProtocol:url];
		}
		else if (buttonIndex == 1) 
		{
			if ( [lamoselDict respondsToSelector:@selector(setObject:forKey:)] ) {
				[lamoselDict setObject:[NSNumber numberWithBool:YES] forKey:@"VMV_NeverAskVoting"];
			}
		}
		
		isAlreadyDoingSomething = NO;
	}
	
	[self saveData];
}

- (void)callURLIfInternalProtocol:(NSString *)url 
{
	if (vmvDelegate != nil) 
	{
		if ([vmvDelegate conformsToProtocol:@protocol(SGMobileServerVMVDelegate)]) 
		{
			if ([vmvDelegate respondsToSelector:@selector(reactToURLSchemeCall:)]) 
			{
				[vmvDelegate reactToURLSchemeCall:url];
			}
		}
	}
}

#pragma mark - MD5

- (void)timerFireMethod:(NSTimer *)theTimer 
{
    if ([SGLAMOSELMainBridge sharedMainBridge].debugLogIsActive == TRUE)
    {
        NSLog(@"\n\nSGMobileServerVMV : timerFireMethod == repeatingTimer");
    }
	
	if (repeatingTimer != nil)
	{
		[repeatingTimer invalidate];
		self.repeatingTimer = nil;
		
		if (vmv == nil)
		{
			return;
		}
		
		if (![vmv count])
		{
			return;
		}
		
		if (!versioningShown) 
		{
            if ([SGLAMOSELMainBridge sharedMainBridge].debugLogIsActive == TRUE)
            {
                NSLog(@"timerFireMethod == versioningShown");
            }
			
			NSNumber *n = [self checkJSON:lamoselDict forKey:@"VMV_NeverAskVoting" defaultValue:[NSNumber numberWithBool:NO]];
			
			if ( ![n boolValue] ) 
			{ // Checking if the user clicked on never ask me again
				NSDate *fl = [self checkJSON:lamoselDict forKey:@"VMV_FirstLaunch" defaultValue:[NSDate dateWithTimeIntervalSince1970:0]];
				NSDate *now = [NSDate date];
				if (
					([(NSDate *)[fl dateByAddingTimeInterval:minYTime] compare:(NSDate *)now] == NSOrderedAscending) &&
					([now compare:(NSDate *)[fl dateByAddingTimeInterval:maxZtime]] == NSOrderedAscending)
				)
				{
					// We are in between the wanted interval
					// We have to check if qualification is right
					NSNumber *n = [self checkJSON:lamoselDict forKey:@"VMV_Qualified" defaultValue:[NSNumber numberWithInt:-1]];
                    
                    if ([SGLAMOSELMainBridge sharedMainBridge].debugLogIsActive == TRUE)
                    {
                        NSLog(@"timerFireMethod == %lu", ([n longValue] % qualifiedOpenings));
                    }
					
					if ([n intValue] > 0 && ([n intValue] % qualifiedOpenings) == 0 ) 
					{
						NSArray *a = [self checkJSON:vmv forKey:@"voting" defaultValue:[NSArray array]];
						
						if ( [a count] ) 
						{
							if ( [[a objectAtIndex:0] isKindOfClass:[NSDictionary class]] ) 
							{
								[self showVoting:[a objectAtIndex:0]];
							}
						}
					}
				}
			}
		}
	}
}

- (void)qualifiedFireMethod:(NSTimer *)theTimer 
{
    if ([SGLAMOSELMainBridge sharedMainBridge].debugLogIsActive == TRUE)
    {
        NSLog(@"\n\nSGMobileServerVMV : qualifiedFireMethod\n\n");
    }
    
	if (qualifiedTimer != nil)
	{
		[qualifiedTimer invalidate];
		self.qualifiedTimer = nil;
		
		NSNumber *n = [self checkJSON:lamoselDict forKey:@"VMV_Qualified" defaultValue:[NSNumber numberWithInt:-1]];
		n = [NSNumber numberWithInt:([n intValue]+1)];
		
		if ( [lamoselDict respondsToSelector:@selector(setObject:forKey:)] )
        {
			[lamoselDict setObject:n forKey:@"VMV_Qualified"];
		}
		
		[self saveData];
		
        if ([SGLAMOSELMainBridge sharedMainBridge].debugLogIsActive == TRUE)
        {
            NSLog(@"\n\nSGMobileServerVMV : qualifiedFireMethod = %@\n\n", n);
        }		
	}
}

- (id)checkJSON:(NSDictionary *)d forKey:(id)key defaultValue:(id)defaultValue 
{
	return (([d objectForKey:key] == nil)||([d objectForKey:key]==[NSNull null])) ? defaultValue : [d objectForKey:key];
}

+ (NSString *)md5:(NSString *)str 
{
	const char *cStr = [str UTF8String];
	unsigned char result[16];
	CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
	
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3], 
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			]; 
}
#pragma clang diagnostic pop

@end
