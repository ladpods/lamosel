//
//  UIURLAlertView.h
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 16/09/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIURLAlertView : UIAlertView 
{
	NSString *url;
}

@property (nonatomic, retain) NSString *url;

@end
