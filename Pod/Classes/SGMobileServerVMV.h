//
//  SGMobileServerVMV.h
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 26/10/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SGMobileServerBridge.h"

@class UIURLAlertView;

@protocol SGMobileServerVMVDelegate <NSObject>

@optional

- (void)reactToURLSchemeCall:(NSString *)url;

//add option in  Version 1.6

- (NSString*)getLanguageCode;

@end

typedef enum 
{
	PopUpVMVVersioningTag = 1,
	PopUpVMVMessagingTag = 2,
	PopUpVMVVotingTag = 3
    
} PopUpVMVTagType; 

@interface SGMobileServerVMV : SGMobileServerBridge <UIAlertViewDelegate> 
{
	NSTimer                             *repeatingTimer, *qualifiedTimer;
	NSDictionary                        *vmv;
	NSString                            *currentVersion;
	UIURLAlertView                      *alerting;
	NSMutableDictionary                 *lamoselDict;
	Boolean                             versioningShown, isAlreadyDoingSomething;
	id<SGMobileServerVMVDelegate>       vmvDelegate;
	
	NSUInteger                          timeBeforePopUp, fifoMessagesLimit, qualifiedOpenings, minYTime, maxZtime, versioningRecurrence;
    
    BOOL                                mustSkipcacheOnVMVCall;
}

@property (assign)              NSTimer                             *repeatingTimer, *qualifiedTimer;
@property (nonatomic, retain)   NSDictionary                        *vmv;
@property (nonatomic, retain)   NSString                            *currentVersion;
@property (nonatomic, retain)   UIURLAlertView                      *alerting;
@property (nonatomic, retain)   NSMutableDictionary                 *lamoselDict;
@property (nonatomic, assign)   id<SGMobileServerVMVDelegate>       vmvDelegate;
@property (nonatomic, assign)   NSUInteger                          timeBeforePopUp, fifoMessagesLimit, qualifiedOpenings, minYTime, maxZtime, versioningRecurrence;

@property (nonatomic, assign)   BOOL                                mustSkipcacheOnVMVCall;

- (void)loadData;
- (void)saveData;

#pragma mark - Rules - Versioning

- (Boolean)aNewVersionExist;
- (Boolean)versioningForCurrentVersionAlreadyPrinted;
- (Boolean)messageForCurrentVersionAlreadyPrinted:(NSDictionary *)message;

#pragma mark - Launch

- (void)showVersioning;
- (void)showMessaging:(NSDictionary *)message;
- (void)showVoting:(NSDictionary *)voting;

- (void)start;
- (void)startVotingTimer;
- (void)startQualifiedTimer;
- (void)startUserVMV:(NSNumber *)contentObjectIdentifier currentVersion:(NSString *)cVersion;
- (void)stopUserVMV;

- (NSInteger)stepMessaging;
- (Boolean)stepVoting;

#pragma mark - Versioning-Messaging-Voting

- (void)baseVersioningMessagingVoting:(NSNumber *)contentObjectIdentifier currentVersion:(NSString *)cVersion;

#pragma mark - UIAlertView delegate

- (void)callURLIfInternalProtocol:(NSString *)url;

#pragma mark - MD5

- (void)timerFireMethod:(NSTimer*)theTimer;
- (id)checkJSON:(NSDictionary *)d forKey:(id)key defaultValue:(id)defaultValue;
+ (NSString *)md5:(NSString *)str;

@end
