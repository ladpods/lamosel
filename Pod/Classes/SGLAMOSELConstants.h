//
//  SGLAMOSELConstants.h
//  LAMOSEL
//  Version 2.0.3 
//  Created by Lagardere Active Digital on 02/06/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import <Foundation/Foundation.h>

static const    NSString    *LAMOSEL_SDK_VERSION            = @"2.0.3";

// LAMOSEL TIME CONSTANTS
static const    long        LAMOSEL_TIME_MINUTE             = 60;
static const    long        LAMOSEL_TIME_HOUR               = (60 * LAMOSEL_TIME_MINUTE);
static const    long        LAMOSEL_TIME_DAY                = (24 * LAMOSEL_TIME_HOUR);
static const    long        LAMOSEL_TIME_5_DAYS             = (5 * LAMOSEL_TIME_DAY);
static const    long        LAMOSEL_TIME_WEEK               = (7 * LAMOSEL_TIME_DAY);
static const    long        LAMOSEL_TIME_MONTH              = (30.5 * LAMOSEL_TIME_DAY);
static const    long        LAMOSEL_TIME_YEAR               = (365 * LAMOSEL_TIME_DAY);

// LAMOSEL_VMV_CONSTANTS

static const    long        kTimeBeforePopUp                = LAMOSEL_TIME_MINUTE;
static const    long        kFIFOMessagesLimit              = 15;
static const    long        kQualifiedOpenings              = 5;
static const    long        kMinYTime                       = (2 * LAMOSEL_TIME_DAY);
static const    long        kMaxZTime                       = (40 * LAMOSEL_TIME_DAY);
static const    long        kVersioningRecurrence           = LAMOSEL_TIME_WEEK;


// ----------------------------------------------------------------------------
// URLs
// ----------------------------------------------------------------------------

static const    NSString    *kMobileServerBaseURL           = @"https://mobiles.lagardere-active.com";
static const    NSString    *kMobileServerBaseURLStaging    = @"http://pp.mobiles.lagardere-active.com";

// ----------------------------------------------------------------------------
// Common Lagardere Digital France Web Services
// ----------------------------------------------------------------------------

static const    NSString       *kMobileServerWSVMVInfo      = @"/version/infos";
static const    long            kMobileServerWSTimeout      = 25.0;

typedef enum
{
	MSAPNSSendDeviceToken				= 0x1001,
	MSAPNSSendDeviceTokenWithServices	= 0x1002,
	MSGeneralDeviceInfo					= 0x2001,
	MSGeneralApplicationInfo			= 0x2002,
	MSAPNSAddPush						= 0x2003,
	MSAPNSDeletePush					= 0x2004,
	MSAPNSMassivePush					= 0x2005,
	MSInAppStoreListProducts			= 0x3001,
	MSInAppStoreCompleteTransaction		= 0x3002,
	MSInAppStoreConsumeProduct			= 0x3003,
	MSInAppStoreRemainingProducts		= 0x3004,
	MSInAppStoreVerifyReceipt			= 0x3005,
	MSVMVInfo							= 0x4001
} LAMOSELService;

// Error Codes

typedef enum
{
	PR_STATUS_OK 								=  0,
	PR_STATUS_KO_BAD_APPLICATION_BUNDLE_ID 		= -1,
	PR_STATUS_KO_BAD_ENTITIES 					= -2,
	PR_STATUS_KO_UNKNOWN_RECEIVER_GUID			= -3,
	PR_STATUS_KO_UNKNOWN_SEND_GUID				= -4,
	PR_STATUS_KO_DEACTIVATED_DEVICE_TOKEN		= -5,
	PR_STATUS_KO_DATE_ISO8601_COMPLIANT			= -6,
	PR_STATUS_KO_UNKNOWN_ENTITY_GUID			= -7,
	PR_STATUS_KO_UNKNOWN_ERROR			 		= -8,
	PR_STATUS_KO_MANDATORY_VARIABLE_REQUIRED	= -9,
	PR_STATUS_KO_MESSAGE_LENGTH_EXCEEDED		= -10,
	PR_STATUS_KO_BAD_RECURRENCE_CONFIG			= -11,
	PR_STATUS_KO_BAD_RECURRENCE_FORMAT			= -12,
	PR_STATUS_KO_NO_ENTITY						= -13,
	PR_STATUS_KO_BAD_DEVICE_TOKEN_FORMAT 		= -14,
	PR_STATUS_KO_BAD_TRANSACTION_IDENTIFIER		= -15,
	PR_STATUS_KO_DATA_INCONSISTENCY				= -16,
	PR_STATUS_KO_BAD_SECRET_KEY					= -17,
	PR_STATUS_KO_BAD_FILE						= -18,
	PR_STATUS_KO_BAD_SESSION					= -19,
	PR_STATUS_KO_BAD_SYNTAX						= -20,
	PR_STATUS_KO_UNKNOWN_SERVICE				= -21
    
} LAMOSELErrorCode;

