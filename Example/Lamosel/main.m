//
//  main.m
//  Lamosel
//
//  Created by Stéphane Couzinier on 10/30/2015.
//  Copyright (c) 2015 Stéphane Couzinier. All rights reserved.
//

@import UIKit;
#import "LAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LAAppDelegate class]));
    }
}
