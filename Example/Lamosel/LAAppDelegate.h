//
//  LAAppDelegate.h
//  Lamosel
//
//  Created by Stéphane Couzinier on 10/30/2015.
//  Copyright (c) 2015 Stéphane Couzinier. All rights reserved.
//

@import UIKit;

@interface LAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
