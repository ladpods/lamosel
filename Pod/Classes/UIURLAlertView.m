//
//  UIURLAlertView.m
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 16/09/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import "UIURLAlertView.h"


@implementation UIURLAlertView

@synthesize url;

- (void)dealloc 
{
	[url release];
    [super dealloc];
}


@end
