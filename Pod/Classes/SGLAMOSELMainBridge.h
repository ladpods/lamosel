//
//  SGLAMOSELMainBridge.h
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 07/06/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import <Foundation/Foundation.h>

@class  SGMobileServerVMV;

@interface SGLAMOSELMainBridge : NSObject 
{
	SGMobileServerVMV					*bridgeVMV;
	
	BOOL								debugLogIsActive;
	BOOL								isStagingMode;
}

@property (nonatomic, strong) SGMobileServerVMV						*bridgeVMV;

@property (nonatomic, assign) BOOL									debugLogIsActive;
@property (nonatomic, assign) BOOL									isStagingMode;

/** Singleton object methods */

/// Get Singleton Instance
+ (SGLAMOSELMainBridge *)sharedMainBridge;


/** LAMOSEL Info Methods */

/// Display parameter info about LAMOSEL Library
- (void)getInfo;

@end
