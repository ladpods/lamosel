//
//  SGLAMOSELMainBridge.m
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 07/06/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import "SGLAMOSELMainBridge.h"
#import "SGMobileServerVMV.h"

@implementation SGLAMOSELMainBridge

@synthesize bridgeVMV;
@synthesize debugLogIsActive;
@synthesize isStagingMode;

#pragma mark - Singleton definition

#pragma mark - Initialization

+ (SGLAMOSELMainBridge*)sharedMainBridge
{
    static SGLAMOSELMainBridge* sharedMainBridge = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMainBridge = [[self alloc] init];
    });
    
    return sharedMainBridge;
}

- (id)init 
{
    if (self = [super init]) 
	{
		SGMobileServerVMV *msv = [[SGMobileServerVMV alloc] init];
		[self setBridgeVMV:msv];
        [msv release];
		
		[self setIsStagingMode:FALSE];
		[self setDebugLogIsActive:FALSE];		
	}
	
    return self;
}

#pragma mark - LAMOSEL Info Methods

- (void)getInfo
{
	NSLog(@"\n**********************************************************************************************\n\n");
	NSLog(@"***********************************************");
	NSLog(@"-LAMOSEL => SGLAMOSELMainBridge : getInfo");
	NSLog(@"***********************************************");
	
	NSLog(@"Version %@",LAMOSEL_SDK_VERSION);
		
	if(isStagingMode == TRUE)
	{
		NSLog(@"\n\n--- LAMOSEL : SGMobileServerBridge is in STAGING (TEST) mode ---\n\n");
	}
	else
	{
		NSLog(@"\n\n--- LAMOSEL : SGMobileServerBridge is in LIVE (PROD) mode ---\n\n");
	}

	NSLog(@"-- LAMOSEL => kMobileServerBaseURL: %@ \n\n",kMobileServerBaseURL);
	NSLog(@"-- LAMOSEL => debugLogIsActive: %i | TRUE: %i\n\n",debugLogIsActive, TRUE);
	NSLog(@"-- LAMOSEL => kMobileServerBaseURL: %i | TRUE : %i  \n\n",isStagingMode, TRUE);
	
	NSLog(@"\n**********************************************************************************************\n\n");
}

#pragma mark - Memory Management


@end
