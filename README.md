# Lamosel

[![CI Status](http://img.shields.io/travis/Stéphane Couzinier/Lamosel.svg?style=flat)](https://travis-ci.org/Stéphane Couzinier/Lamosel)
[![Version](https://img.shields.io/cocoapods/v/Lamosel.svg?style=flat)](http://cocoapods.org/pods/Lamosel)
[![License](https://img.shields.io/cocoapods/l/Lamosel.svg?style=flat)](http://cocoapods.org/pods/Lamosel)
[![Platform](https://img.shields.io/cocoapods/p/Lamosel.svg?style=flat)](http://cocoapods.org/pods/Lamosel)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Lamosel is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Lamosel"
```

## Author

Stéphane Couzinier, stephane.couzinier@lagardere-active.com

## License

Lamosel is available under the MIT license. See the LICENSE file for more info.



## ChangeLog

== Version 2.0.3 ==

Use protocol https for serveur url 

== Version 2.0.2 ==

- update message logging

== Version 2.0 ==

- Migration git + pod
- Singleton SGLAMOSELMainBridge

== Version 1.9 ==

- Modification format URL VMV new template is :

http://mobiles.lagardere-active.com/version/infos/#APP_IDENTIFIER#/(bundle)/#APP_BUNDLE_ID#

ex : http://mobiles.lagardere-active.com/version/infos/19805/(bundle)/com.lagardere.ovulation

== Version 1.8 ==

- Version arm64
- Delete any reference to IDFA and AdSupport Framework
- Deprecate "getAdvertiserIdentifier" method => now return an md5 of mac address is available (iOS < 7.0)
- Remove any class releated to Push notification management
- Remove any class releated to In app purchase management
- Remove dependance to TouchJSON and replace JSON Parser by Apple JSON Parser "NSJSONSerialization"
- Exclude Lamosel Configuration file in cache from iCloud backup

== Version 1.6.1 ==

Version for logitech with no reference to IDFA (change not commit)

== Version 1.6 ==

- add option language in vmv menu
- Version number increment to 1.6 in all header file
- add LAMOSEL string in Spanish and Italian in LAMOSEL BUNDLE


== Version 1.5 ==

- update Advertising identifier management


== Version 1.4 ==

- Add support for Unique Vendor Identifier and advertiser Identifier for iOS 7 which disgrace udid based on  mac address
#File :SGMobileServerBridge.h/.m add the following methods :

- (NSString *)getIdentifierForVendor; // return app Identifier for Vendor 			(available since iOS 6 and based on first two string inside bundle 					identifier),if call in < iOS 6 return string getLADDeviceIdentifier

- (NSString *)getAdvertiserIdentifier; // return unique Advertiser Identifier 		provide by AdSupport Framework, available only since iOS 6 in other case return 	getLADDeviceIdentifier

- (BOOL)is_iOS_6_Or_Later; // check if we are one a system runing iOS 6 or later



- Version number increment to 1.4 in all header file


== Version 1.3.8 ==

- add multilingual for Voting, Versioning et Messaging system

- Delete debug symbol on static library generation for remove warning message inside app integration

== Version 1.3.7 ==

- log update
- add variable skip cache for debug only inside SGMobileServerVMV

== Version 1.3.6 ==

- add staging system (preprod platform)

== Version 1.3.5 ==

- crash correction

== Version 1.3.4 ==

=> add architecture support for armv7/armv7s

== Version 1.3.3 ==

- iOS 5 guideline
- webservice update
