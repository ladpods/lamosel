//
//  SGMobileServerBridge.h
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 02/06/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SGLAMOSELConstants.h"

@protocol SGMobileServerBridgeDelegate <NSObject>

@optional
- (void)mobileServerBridgeServiceCallType:(NSInteger)serviceType 
					   didSucceedWithInfo:(NSDictionary *)userInfo;

- (void)mobileServerBridgeServiceCallType:(NSInteger)serviceType 
						 didFailWithError:(NSError *)error;

@end


@interface SGMobileServerBridge : NSObject 
{
	NSMutableDictionary					*receivedData, *connections, *serviceTypes, *ai;
	id<SGMobileServerBridgeDelegate>	delegate;
}

@property (nonatomic, retain)	NSMutableDictionary					*receivedData, *connections, *serviceTypes, *ai;
@property (nonatomic, assign)	id<SGMobileServerBridgeDelegate>	delegate;

#pragma mark - Connections makers

+ (NSString *)baseServerURL;

+ (NSString *)key:(id)object;

- (void)errorMessage:(NSString *)title errorMessage:(NSString *)message;

- (void)launchConnectionForURLRequest:(NSMutableURLRequest *)urlRequest 
						  withService:(NSInteger)service
						   errorTitle:(NSString *)title
						 errorMessage:(NSString *)message
					activityIndicator:(Boolean)indicator;


#pragma mark - Services base reaction support

- (id)treatServiceResult:(NSInteger)serviceType currentConnection:(NSURLConnection *)connection;

#pragma mark - Error Messages

+ (NSError *)errorMessageWithServiceCallResult:(NSDictionary *)serviceCallResult;

+ (NSString *)dateToISO8601:(NSDate*)aDate;

+ (NSString *)stringByURLEncodingString:(NSString *)aString;

#pragma mark - LAD UDID Methods

/// Return the local MAC address
- (NSString *)getMacAddress;

/// Get LAD Unique Device Identifier
- (NSString *)getLADDeviceIdentifier __attribute__((deprecated("Use getIdentifierForVendor or getAdvertiserIdentifier instead"))); // Deprecated in LAMOSEL v1.4,  Use getIdentifierForVendor or getAdvertiserIdentifier instead.

// return app Identifier for Vendor (available since iOS 6 and based on first two string inside bundle identifier),if call in < iOS 6 return string getLADDeviceIdentifier
/// @see http://www.doubleencore.com/2013/04/unique-identifiers/
- (NSString *)getIdentifierForVendor;


- (NSString *)getAdvertiserIdentifier __attribute__((deprecated("Use getIdentifierForVendor instead, this function will now everytime try to return an md5 of mac address if available")));

// check if we are one a system runing iOS 6 or later
- (BOOL)is_iOS_6_Or_Later;

- (NSString *)getUniqueDeviceIdentifierBeforeiOS6; // for internal use only

@end
