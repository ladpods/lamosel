//
//  NSMutableArray+QueueAdditions.h
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 28/10/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSMutableArray (QueueAdditions)
- (id)dequeue;
- (void)enqueue:(id)obj;
@end
