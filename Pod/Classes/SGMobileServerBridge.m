//
//  SGMobileServerBridge.m
//  LAMOSEL
//  Version 2.0.3
//  Created by Lagardere Active Digital on 02/06/10.
//  Copyright 2010 Lagardere Active Digital. All rights reserved.
//

#import "SGMobileServerBridge.h"
#import "SGLAMOSELMainBridge.h"
#import "UIURLAlertView.h"

#import <CommonCrypto/CommonDigest.h>
#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

@implementation SGMobileServerBridge


@synthesize receivedData, connections, serviceTypes, ai;
@synthesize delegate;

#pragma mark - Initialization

- (id)init 
{
    if (self = [super init]) 
	{
		NSMutableDictionary	*dict = [[NSMutableDictionary alloc] init];
		[self setReceivedData:dict];
		[dict release];
		
		dict = [[NSMutableDictionary alloc] init];
		[self setConnections:dict];
		[dict release];
		
		dict = [[NSMutableDictionary alloc] init];
		[self setServiceTypes:dict];
		[dict release];
		
		dict = [[NSMutableDictionary alloc] init];
		[self setAi:dict];
		[dict release];
		
		[self setDelegate:nil];
	}
	
    return self;
}

#pragma mark - LAD UDID Methods

// Return the local MAC addy
// Courtesy of FreeBSD hackers email list
// Accidentally munged during previous update. Fixed thanks to erica sadun & mlamb.
- (NSString *)getMacAddress;
{    
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) 
	{
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0)
	{
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) 
	{
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0)
	{
		free(buf);
        printf("Error: sysctl, take 2");
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X", 
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return outstring;
}

- (NSString *)md5ForString:(NSString*)aString
{    
    if(aString == nil || [aString length] == 0)
	{
        return nil;
	}
    
    const char *value = [aString UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
	for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++)
	{
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return [outputString autorelease];
}

/// Get LAD Unique Device Identifier
- (NSString *)getLADDeviceIdentifier
{
    NSString *macaddress = [self getMacAddress];
    NSString *uniqueIdentifier = [self md5ForString:macaddress];
    
    return uniqueIdentifier;
}

- (NSString *)getUniqueDeviceIdentifierBeforeiOS6
{
    NSString *macaddress = [self getMacAddress];
    NSString *uniqueIdentifier = [self md5ForString:macaddress];
    
    return uniqueIdentifier;
}

// return app Identifier for Vendor (available since iOS 6 and based on first two string inside bundle identifier),if call in < iOS 6 return string getLADDeviceIdentifier
/// @see http://www.doubleencore.com/2013/04/unique-identifiers/
- (NSString *)getIdentifierForVendor
{
    NSString *uniqueIdentifierForVendor = @"";
    
    if ([self is_iOS_6_Or_Later] == TRUE)
    {
        uniqueIdentifierForVendor = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }
    else
    {
        uniqueIdentifierForVendor = [self getUniqueDeviceIdentifierBeforeiOS6];
    }
    
    return uniqueIdentifierForVendor;    
}

- (NSString *)getAdvertiserIdentifier
{
    NSString *uniqueAdvertiserIdentifier;
    uniqueAdvertiserIdentifier = [self getUniqueDeviceIdentifierBeforeiOS6];
    
    return uniqueAdvertiserIdentifier;
}

- (BOOL)is_iOS_6_Or_Later
{
    // available since iOS 6 and Later
    return [[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)];
}

#pragma mark - Memory Management

- (void)dealloc 
{
	if ( delegate != nil )
    {
        delegate = nil;
    }
	
	[ai release];
	[serviceTypes release];
	[receivedData release];
	[connections release];
	
	[super dealloc]; 
}

#pragma mark - Connections makers

+ (NSString *)baseServerURL
{
    if ([[SGLAMOSELMainBridge sharedMainBridge] isStagingMode] == TRUE)
    {
        return [NSString stringWithFormat:@"%@",kMobileServerBaseURLStaging];
    }
    else
    {
        return [NSString stringWithFormat:@"%@",kMobileServerBaseURL];
    }
}

+ (NSString *)key:(id)object
{
	return [NSString stringWithFormat:@"%p", object];
}

- (void)errorMessage:(NSString *)title errorMessage:(NSString *)message 
{
	UIAlertView *baseAlert = [[UIAlertView alloc] initWithTitle:( ( title != nil ) ? title : NSLocalizedString(@"Error", @"") )
														message:( ( message != nil ) ? message : NSLocalizedString(@"NoConnexion", @"") )
													   delegate:self 
											  cancelButtonTitle:nil 
											  otherButtonTitles:@"OK", nil];
	[baseAlert show];
	[baseAlert release];
}

- (void)launchConnectionForURLRequest:(NSMutableURLRequest *)urlRequest withService:(NSInteger)service  errorTitle:(NSString *)title errorMessage:(NSString *)message activityIndicator:(Boolean)indicator
{
	if ([[SGLAMOSELMainBridge sharedMainBridge] isStagingMode] == TRUE)
	{
//#ifdef STAGING
//		[urlRequest addValue:@"Basic aGZwOmhmcDA3" forHTTPHeaderField:@"Authorization"];
//#endif
	}
	
	NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
	
	if (connection) 
	{
		if (indicator) 
		{
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];	
		}
		
		if ( [ai respondsToSelector:@selector(setObject:forKey:)] ) {
			[ai setObject:[NSNumber numberWithBool:indicator] forKey:[SGMobileServerBridge key:connection]];
		}
		
		if ( [serviceTypes respondsToSelector:@selector(setObject:forKey:)] ) {
			[serviceTypes setObject:[NSNumber numberWithLong:service] forKey:[SGMobileServerBridge key:connection]];
		}
		
		if ( [connections respondsToSelector:@selector(setObject:forKey:)] ) {
			[connections setObject:connection forKey:[SGMobileServerBridge key:connection]];
		}
		
		if ( [receivedData respondsToSelector:@selector(setObject:forKey:)] ) {
			[receivedData setObject:[NSMutableData data] forKey:[SGMobileServerBridge key:connection]];
		}
	} 
	else 
	{
		[self errorMessage:title errorMessage:message];
	}
	
	[connection release];
}

#pragma mark - NSURLConnections

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response 
{
	[(NSMutableData*)[receivedData objectForKey:[SGMobileServerBridge key:connection]] setLength:0];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"SGMobileServerBridgeDidConnectSuccesfully" 
														object:self 
													  userInfo:[NSDictionary dictionaryWithObject:response forKey:@"NSURLResponse"]];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection 
{
	
#ifdef STAGING
	NSString* aStr = [[NSString alloc] initWithData:[receivedData objectForKey:[NSString stringWithFormat:@"%p", connection]] encoding:NSUTF8StringEncoding];
	//NSLog(@"\nconnectionDidFinishLoading\n%@\n", aStr);
	[aStr release];
#endif
	
	LAMOSELService st = [[serviceTypes objectForKey:[SGMobileServerBridge key:connection]] intValue];
	[self treatServiceResult:st currentConnection:connection];
	
	if ([[ai objectForKey:[SGMobileServerBridge key:connection]] boolValue]) 
	{
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];	
	}
	
	[receivedData removeObjectForKey:[SGMobileServerBridge key:connection]];
	[connections removeObjectForKey:[SGMobileServerBridge key:connection]];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data 
{
	[[receivedData objectForKey:[NSString stringWithFormat:@"%p", connection]] appendData:data];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"SGMobileServerBridgeDidReceiveData" 
														object:self 
													  userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithLong:[data length]] forKey:@"DataLength"]];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error 
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[receivedData removeObjectForKey:[SGMobileServerBridge key:connection]];
	[connections removeObjectForKey:[SGMobileServerBridge key:connection]];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"ObjectShouldExit" object:self];
	
	if (delegate != nil) 
	{
		if ([delegate conformsToProtocol:@protocol(SGMobileServerBridgeDelegate)]) 
		{
			if ([delegate respondsToSelector:@selector(mobileServerBridgeServiceCallType:didFailWithError:)]) 
			{
				[delegate mobileServerBridgeServiceCallType:PR_STATUS_KO_UNKNOWN_SERVICE didFailWithError:error];
			}
		}
	}
	
	NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
					   [NSNumber numberWithInt:PR_STATUS_KO_UNKNOWN_SERVICE], @"ServiceType",
					   error, @"Error", 
					   nil];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"SGMobileServerBridgeDidFailRequest" object:self userInfo:d];
}

#pragma mark - Services base reaction support

- (id)treatServiceResult:(NSInteger)serviceType currentConnection:(NSURLConnection *)connection
{
    NSError* error = nil;
    
    NSDictionary* dictionary = [NSJSONSerialization  JSONObjectWithData:[receivedData objectForKey:[SGMobileServerBridge key:connection]]  options:kNilOptions error:&error];
    
    error = [SGMobileServerBridge errorMessageWithServiceCallResult:dictionary];
	
	if ( [error code] == PR_STATUS_OK ) 
	{
		// OK the server returned a STATUS OK response, which is a good thing
		// We now can pass information to the delegate if it exist to tell him the good news
		if (delegate != nil) 
		{
			if ([delegate conformsToProtocol:@protocol(SGMobileServerBridgeDelegate)]) 
			{
				if ([delegate respondsToSelector:@selector(mobileServerBridgeServiceCallType:didSucceedWithInfo:)]) 
				{
					[delegate mobileServerBridgeServiceCallType:serviceType didSucceedWithInfo:dictionary];
				}
			}
		}
		
		if (serviceType == MSInAppStoreRemainingProducts) 
		{
			//We store the remaining product dictionary
			[[NSUserDefaults standardUserDefaults] setObject:[receivedData objectForKey:[SGMobileServerBridge key:connection]]  forKey:@"LAMOSEL_IAP_data"];
			[[NSUserDefaults standardUserDefaults] synchronize];
		}
		
		NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
						   [NSNumber numberWithInt:(int)serviceType], @"ServiceType",
						   error, @"Error",
						   dictionary, @"UserInfo",
						   nil];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"SGMobileServerBridgeDidSucceedRequest" object:self userInfo:d];
	} 
	else 
	{
		// KO the server returned anything else than a STATUS OK response, which is a really bad thing
		// We then inform the delegate if it exist that something bad occured
		if (delegate != nil) 
		{
			if ([delegate conformsToProtocol:@protocol(SGMobileServerBridgeDelegate)]) 
			{
				if ([delegate respondsToSelector:@selector(mobileServerBridgeServiceCallType:didFailWithError:)]) 
				{
					[delegate mobileServerBridgeServiceCallType:serviceType didFailWithError:error];
				}
			}
		}
		
		NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
						   [NSNumber numberWithInt:(int)serviceType], @"ServiceType",
						   error, @"Error",
						   dictionary, @"UserInfo",
						   nil];
		[[NSNotificationCenter defaultCenter] postNotificationName:@"SGMobileServerBridgeDidFailRequest" 
															object:self 
														  userInfo:d];
	}
	
	return nil;
}

#pragma mark - Error Messages

+ (NSError *)errorMessageWithServiceCallResult:(NSDictionary *)serviceCallResult 
{
	NSInteger errorCode = [[serviceCallResult objectForKey:@"status"] intValue];
	NSString *domain = nil;
	
	switch ( errorCode ) 
	{
		case PR_STATUS_OK:
			domain = @"OK";
			break;
		case PR_STATUS_KO_BAD_APPLICATION_BUNDLE_ID:
			domain = @"Unknown application bundle identifier";
			break;
		case PR_STATUS_KO_BAD_ENTITIES:
			domain = @"Unknown specified entities";
			break;
		case PR_STATUS_KO_UNKNOWN_RECEIVER_GUID:
			domain = @"Unknown receiver UID";
			break;
		case PR_STATUS_KO_UNKNOWN_SEND_GUID:
			domain = @"Unknown sender UID";
			break;	
		case PR_STATUS_KO_DEACTIVATED_DEVICE_TOKEN:
			domain = @"The specified device token is considered as inactive";
			break;
		case PR_STATUS_KO_DATE_ISO8601_COMPLIANT:
			domain = @"The specified date format is not ISO8601 compliant :: YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)";
			break;
		case PR_STATUS_KO_UNKNOWN_ENTITY_GUID:
			domain = @"Unknown entity identifier";
			break;
		case PR_STATUS_KO_MANDATORY_VARIABLE_REQUIRED:
			domain = @"A required variable is missing";
			break;
		case PR_STATUS_KO_MESSAGE_LENGTH_EXCEEDED:
			domain = @"The length of the sent message exceed maximum capacity of 256 bytes";
			break;
		case PR_STATUS_KO_BAD_RECURRENCE_CONFIG:
			domain = @"Bad recurrence confiugration. The recurrence configuration is bad, really really bad";
			break;
		case PR_STATUS_KO_BAD_RECURRENCE_FORMAT:
			domain = @"Error in recurrence format, (refer to the documentation)";
			break;
		case PR_STATUS_KO_NO_ENTITY:
			domain = @"No defined entity";
			break;
		case PR_STATUS_KO_BAD_DEVICE_TOKEN_FORMAT:
			domain = @"Bad devite token format";
			break;
		case PR_STATUS_KO_BAD_TRANSACTION_IDENTIFIER:
			domain = @"Band transaction identifier";
			break;
		case PR_STATUS_KO_DATA_INCONSISTENCY:
			domain = @"Data inconsistency found";
			break;
		case PR_STATUS_KO_BAD_SECRET_KEY:
			domain = @"The secret key you gave was invalid";
			break;
		case PR_STATUS_KO_BAD_FILE:
			domain = @"Bad file given";
			break;
		case PR_STATUS_KO_BAD_SESSION:
			domain = @"Bad session found";
			break;
		case PR_STATUS_KO_BAD_SYNTAX:
			domain = @"Bad syntax";
			break;
		case PR_STATUS_KO_UNKNOWN_SERVICE:
			domain = @"Unknown service";
			break;
		case PR_STATUS_KO_UNKNOWN_ERROR:
		default:
			domain = @"Unknown error";
			break;
	}
	
	id res = nil;
	
	if ([serviceCallResult objectForKey:@"payload"] != nil) 
	{
		res = [serviceCallResult objectForKey:@"payload"];
	}
	else 
	{
		res = @"";
	}
	
    if ([SGLAMOSELMainBridge sharedMainBridge].debugLogIsActive == TRUE)
    {
        NSLog(@"\n\n### SGMobileServerBridge : errorMessageWithServiceCallResult => res: %@ ###\n\n", res);
    }
    
	return [NSError errorWithDomain:domain code:errorCode userInfo:nil];
}

+ (NSString *)dateToISO8601:(NSDate*)aDate 
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss#Z"];
	NSString *dateInternational = [formatter stringFromDate:aDate];
	[formatter release];
	
	NSRange range;
	range.length = [dateInternational length]-2;
	range.location = 0;
	
	NSString *endDate  = [dateInternational substringFromIndex:[dateInternational length]-2];
	dateInternational = [dateInternational substringWithRange:range];
	dateInternational = [dateInternational stringByAppendingString:@":"];
	dateInternational = [dateInternational stringByAppendingString:endDate];
	
	NSString *dateFinal = [dateInternational stringByReplacingOccurrencesOfString:@"+" withString:@"%2b"]; 
	dateFinal = [dateFinal stringByReplacingOccurrencesOfString:@" " withString:@"T"];
	dateFinal = [dateFinal stringByReplacingOccurrencesOfString:@"#" withString:@""]; 
	
	return dateFinal;
}

+ (NSString *)stringByURLEncodingString:(NSString *)aString 
{
	NSString *encodedString = (NSString *)CFURLCreateStringByAddingPercentEscapes(
																				  NULL,
																				  (CFStringRef)aString,
																				  NULL,
																				  (CFStringRef)@"!*'();:@&=+$,/?%#[]",
																				  kCFStringEncodingUTF8 );
	return [encodedString autorelease];
}

@end
